from fastai2 import *
from fastai2.vision.all import *
import numpy as np

import wandb
from fastai2.callback.wandb import *
wandb.init(project="nimbusnet")

import os
# os.environ['WANDB_MODE'] = 'dryrun' # run offline

giant_np_dataset_x = np.load('modis_full.npy')
giant_np_dataset_y = np.load('modis_full_labels.npy')

def get_x(x):
  # channels = [giant_np_dataset_x[i, 14], giant_np_dataset_x[i, 26], giant_np_dataset_x[i, 31]]
  # channels = np.array(channels).swapaxes(0,2).swapaxes(0,1)
  # print(channels.shape)
  red = giant_np_dataset_x[x, 14]
  green = giant_np_dataset_x[x, 26]
  blue = giant_np_dataset_x[x, 31]
  channels = np.stack((green, blue, red), axis=2)
  im = PILImage.create(np.uint8(channels))
  return im
def get_y(y):
  # return PILImage.create(np.uint8(giant_np_dataset_y[2, 4]))
  np_msk = np.uint8(giant_np_dataset_y[y]).clip(0,1)
  msk = PILMask.create(np_msk)
  return msk

batch_tfms = [*aug_transforms(size=64, flip_vert=True, max_rotate=180.0, max_zoom=1, max_warp=0, max_lighting=0, p_affine=0, p_lighting=0), Normalize.from_stats(*imagenet_stats)]


data = DataBlock(
    blocks=(ImageBlock(PILImage), ImageBlock(PILMask)),
    splitter=RandomSplitter(),
    get_x=get_x,
    get_y=get_y,
    batch_tfms=None
)

dls = data.dataloaders(range(52000), bs=128)
dls.vocab = ['Cloudy', 'Clear']
dls.show_batch()

def pixelwise_acc(inp, targ):
  void_code = 100
  targ = targ.squeeze(1)
  mask = targ != void_code
  return (inp.argmax(dim=1)[mask]==targ[mask]).float().mean()
  # return (inp.argmax(dim=1)[targ] == targ).float().mean()

def cloud_frac_diff(inp, targ):
  return abs(torch.sigmoid(inp.float()).mean() - targ.float().mean())

config = unet_config(self_attention=True, act_cls=Mish)
opt = ranger
#wandb = WandbCallback(input_type='images')
metrics = [Dice(), JaccardCoeff(), pixelwise_acc, cloud_frac_diff]
learn = unet_learner(dls, xresnet18, pretrained=True,  metrics=metrics, config=config, opt_func=ranger, cbs=WandbCallback())
learn.fit_flat_cos(20)
learn.unfreeze()
learn.fit_flat_cos(20)
learn.save('/home/aua2/nimbusnet/stage-1')
learn.export()