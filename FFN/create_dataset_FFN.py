import numpy as np
import glob
import h5py
import pickle

# create train and test sets
folder = '/home/hackathon/output_64_Javier_labelled/'
img_list = glob.glob(folder + "/*.hdf", recursive=False)

# create global matrix here

# DO NOT FORGET TO REMOVE FIRST ROW !
final_transform = np.empty((1, 45))


for index, img in enumerate(img_list):
    print(index, img)
    
    try:
        hf_file = h5py.File(img, 'r')
    except Exception as ex:
        continue

    # grab h5py file object
    hf_file = h5py.File(img, 'r')

    # list the main groups; image number in this case
    hf_keys = list(hf_file.keys())
    
    for image_num in hf_keys:
        # create temp matrix here
        # skip hdf file if Classification_Accuracy is -1
        Classification_Accuracy = hf_file[image_num + '/ClassificationAccuracy'][()]
        Feature_Labels          = hf_file[image_num + '/FeatureLabels'][()]
        Image_Classification    = hf_file[image_num + '/ImageClassification'][()]
        Image_Features          = hf_file[image_num + '/ImageFeatures'][()]

        clf_array = np.ones((4096,1)) * Classification_Accuracy

        if(Classification_Accuracy == 0 or Classification_Accuracy == -1):
            continue
        else:
            clf_reshape = Image_Classification.reshape(4096,1)
            feat_reshape = Image_Features.reshape(4096, 43)

            transf1 = np.append(feat_reshape, clf_reshape, axis=1)
            transf2 = np.append(transf1, clf_array, axis=1)
        
        # append temp to global
        final_transform = np.append(final_transform, transf2, axis=0)

        if(index%50 == 0):
            # print(final_transform.shape)
            print("Shape: ", tuple(np.subtract(final_transform.shape, (1,0))))

# remove the first placeholder row
final_transform = final_transform[1:]
print("Final Shape: ", final_transform.shape)
print('\n Writing to transform.pkl')

with open('transform.pkl', 'wb') as outfile:
   pickle.dump(final_transform, outfile)
