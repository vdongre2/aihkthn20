import collections
import time

import numpy as np
import pandas as pd
import torch
import pickle
from sklearn.preprocessing import label_binarize
from torch.utils.data import DataLoader
from sklearn.metrics import confusion_matrix

# ensure reproducibility of results
seed = 42
np.random.seed(seed)
torch.manual_seed(seed)

class DatasetFromNumpy():
    
    def __init__(self, np_data):
        self.feature_labels = [
                "Band_8.0",
                "Band_9.0",
                "Band_10.0",
                "Band_11.0",
                "Band_12.0",
                "Band_13.0",
                "Band_13.5",
                "Band_14.0",
                "Band_14.5",
                "Band_15.0",
                "Band_16.0",
                "Band_17.0",
                "Band_18.0",
                "Band_19.0",
                "Band_26.0",
                "Band_20.0",
                "Band_21.0",
                "Band_22.0",
                "Band_23.0",
                "Band_24.0",
                "Band_25.0",
                "Band_27.0",
                "Band_28.0",
                "Band_29.0",
                "Band_30.0",
                "Band_31.0",
                "Band_32.0",
                "Band_33.0",
                "Band_34.0",
                "Band_35.0",
                "Band_1.0",
                "Band_2.0",
                "Band_3.0",
                "Band_4.0",
                "Band_5.0",
                "Band_6.0",
                "Band_7.0",
                "SolarZenithAngle",
                "SensorZenithAngle",
                "RelativeAzimuthAngle",
                "Latitude",
                "Longitude",
                "DayOfYear",
                "MODISGroundTruth",
                "ExpertQualityCheck"
            ]

        self.data = pd.DataFrame.from_records(np_data)
        self.data.columns = self.feature_labels
        # self.data = self.data.astype({"DayofYear": 'int32', "MODISGroundTruth": 'int32'})
        self.data_len = len(self.data.index)
        return
    
    # get (x,y) sample at a particular index in the csv
    def __getitem__(self, index):
        
        x = torch.from_numpy(np.asarray(self.data.iloc[index][:-2])).float()
        label = int(self.data.iloc[index]["MODISGroundTruth"])
        # certain cloud: class 1
        if label == 0:
            y = 1
        # everything else: class 0
        else:
            y = 0
        return (x, y)
    
    def __len__(self):
        return self.data_len

    
class Simple_CNN(torch.nn.Module):
    def __init__(self, input_dim, output_dim, in_channels, hyperparams, padding=0, stride=1):
        super(Simple_CNN, self).__init__()
        
        self.input_dim = input_dim
        self.output_dim = output_dim
        self.kernel_size = hyperparams["kernel_size"]
        self.out_channels = hyperparams["out_channels"]
        
        # # define the CNN block
        # self.conv_block_1 = torch.nn.Sequential(collections.OrderedDict([
        #     ("conv1d_1", torch.nn.Conv1d(in_channels=in_channels, out_channels=self.out_channels, kernel_size=self.kernel_size, stride=stride, padding=padding, bias=False)),
        #     # ("batchnorm", torch.nn.BatchNorm1d(self.out_channels)),
        #     ("relu", torch.nn.ReLU(inplace=True)),
        #     # ("dropout", torch.nn.Dropout2d(p=0.3)),
            
        #     ("conv1d_2", torch.nn.Conv1d(in_channels=self.out_channels, out_channels=self.out_channels, kernel_size=self.kernel_size, stride=stride, padding=padding, bias=False)),
        #     # ("batchnorm", torch.nn.BatchNorm1d(self.out_channels)),
        #     ("relu", torch.nn.ReLU(inplace=True)),
        #     # ("dropout", torch.nn.Dropout2d(p=0.1)),
            
        #     ("conv1d_3", torch.nn.Conv1d(in_channels=self.out_channels, out_channels=self.out_channels, kernel_size=self.kernel_size, stride=stride, padding=padding, bias=False)),
        #     ("batchnorm", torch.nn.BatchNorm1d(self.out_channels)),
        #     ("relu", torch.nn.ReLU(inplace=True)),
        #     ("dropout", torch.nn.Dropout2d(p=0.3)),
            
        #     # ("avg_pool", torch.nn.MaxPool2d(kernel_size=2, stride=2, padding=0))
        #     ("max_pool", torch.nn.MaxPool2d(kernel_size=2, stride=2))
        # ]))
        
        # define the fully connected block
        self.fc_block = torch.nn.Sequential(collections.OrderedDict([
            ("fc_1", torch.nn.Linear(in_features=self.input_dim, out_features=500, bias=True)),
            # ("fc_2", torch.nn.Linear(in_features=500, out_features=200, bias=True)),
            ("fc_3", torch.nn.Linear(in_features=500, out_features=200, bias=True)),
            ("dropout", torch.nn.Dropout(p=0.3)),
            ("relu_1", torch.nn.ReLU(inplace=True)),
            # ("linear", torch.nn.Linear(in_features=100, out_features=20, bias=True)),
            ("linear", torch.nn.Linear(in_features=200, out_features=self.output_dim, bias=True))
            
        ]))
        
        # Xavier initialization scheme
        # self.conv_block_1.apply(lambda x: torch.nn.init.xavier_normal_(x.weight, gain=3) if type(x) == torch.nn.Conv1d else None)
        self.fc_block.apply(lambda x: torch.nn.init.xavier_normal_(x.weight, gain=3) if type(x) == torch.nn.Linear else None)
        
        # choice of loss function
        self.loss_function = torch.nn.CrossEntropyLoss()
        
        print ("** Simple CNN model initialized with hyperparameters: ", hyperparams)
        return
    
    def forward(self, x_minibatch):
        # x = self.conv_block_1(x_minibatch)
        # (_, C, L) = x.data.size()
        # x = x.view( -1, C * L)
        outputs = self.fc_block(x_minibatch)
        return (outputs)
    
    def fit(self, train_loader, test_loader, hyperparams):
        time1 = time.time()
        
        optimizer = torch.optim.Adam(self.parameters(), lr=hyperparams["learning_rate"])
        scheduler = torch.optim.lr_scheduler.MultiStepLR(optimizer, milestones=hyperparams["milestones"], gamma=0.1)
        
        for epochs in range(hyperparams["num_epochs"]):
            self.train()
            total_correct = 0
            for batch_id, (x_minibatch, y_minibatch) in enumerate(train_loader):

                # send minibatch to GPU
                x_minibatch = x_minibatch.to(device)
                y_minibatch = y_minibatch.to(device)

                optimizer.zero_grad()

                # forward pass
                outputs = self.forward(x_minibatch)
                loss = self.loss_function(outputs, y_minibatch)
                
                # backward pass
                loss.backward()

                # bluewaters patch for ADAM optimizer
                # if(epochs > 6):
                #     for group in optimizer.param_groups:
                #         for p in group['params']:
                #             state = optimizer.state[p]
                #             if 'step' in state.keys():
                #                 if(state['step']>=1024):
                #                     state['step'] = 1000

                # update parameters
                optimizer.step()
                
                # print("outputs.data: ", outputs.data)
                _, predictions = torch.max(outputs.data, 1)
                # print("predictions: ", predictions)
                # print("y_minibatch: ", y_minibatch)
                total_correct += (predictions == y_minibatch).sum().item()
                
            scheduler.step()
            # TODO: add metrics for binary classification
            training_accuracy = total_correct/np.float(len(train_loader.dataset))
            print("---- Training Accuracy after Epoch #", epochs+1, " : ", training_accuracy)
            
            test_acc = self.accuracy(test_loader)

        time2 = time.time()
        print("** Time taken for Training (minutes): ", (time2-time1)/60.0)
        return
        
    def accuracy(self, test_loader):
        total_correct = 0
        self.eval()
        with torch.no_grad():
            y_true = [ ]
            y_pred = [ ]
            for batch_id, (x_minibatch, y_minibatch) in enumerate(test_loader):
                
                # send minibatch to GPU
                x_minibatch = x_minibatch.to(device)
                y_minibatch = y_minibatch.to(device)
                
                # forward pass
                outputs = self.forward(x_minibatch)
                
                _, predictions = torch.max(outputs.data, 1)
                total_correct += (predictions == y_minibatch).sum().item()
                y_pred = y_pred + predictions.cpu().numpy().tolist()
                y_true = y_true + y_minibatch.cpu().numpy().tolist()
                

        test_acc = total_correct/np.float(len(test_loader.dataset))
        print("** Accuracy on Test Set: ", test_acc)
        
        # (tn, fp, fn, tp)
        tn, fp, fn, tp = confusion_matrix(y_true, y_pred).ravel()
        # normalize confusion matrix over all the cells
        tn, fp, fn, tp = tn/len(y_true), fp/len(y_true), fn/len(y_true), tp/len(y_true)
        print("** TN: %f FP %f FN %f TP %f" % (tn, fp, fn, tp))
        precision = tp / (tp + fp)
        recall = tp / (tp + fn)
        f_score = (2 * precision * recall) / (precision + recall)
        print("** Precision: %f Recall: %f F-Measure: %f" % (precision, recall, f_score))
        return (test_acc)


if __name__ == "__main__":
    
    # use GPU if available
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    print('** Using device:', device)
    print('** Device name: ', torch.cuda.get_device_name(0))
    
    # build and load the train/test datasets
    TRAIN_SPLIT = 0.70
    NUM_FEATURES = 43
    data = pickle.load( open( "./dataset.pkl", "rb" ) )
    print("** Pickle file loaded!")
    data = data[1:]
    data[:,43]
    indices = np.random.permutation(data.shape[0])
    training_idx, test_idx = indices[:int(data.shape[0]*TRAIN_SPLIT)], indices[int(data.shape[0]*TRAIN_SPLIT):]
    np_data_train, np_data_test = data[training_idx,:], data[test_idx,:]
    custom_dataset_train = DatasetFromNumpy(np_data = np_data_train)
    custom_dataset_test = DatasetFromNumpy(np_data = np_data_test)
    
    # TODO: do a weighted sampling for train dataloader
    batch_size = 256
    # class_sample_count = [129689, 341352]
    # weights = 1 / torch.Tensor(class_sample_count)
    # weights = weights.double()
    # sampler = torch.utils.data.sampler.WeightedRandomSampler(weights, batch_size)    
    train_dataloader = DataLoader(custom_dataset_train, batch_size=batch_size, 
                                  shuffle=True, 
                                  num_workers=8, 
                                #   sampler=sampler
                                  )
    test_dataloader = DataLoader(custom_dataset_test, batch_size=batch_size, 
                                 shuffle=False, 
                                 num_workers=8,
                                #   sampler=sampler
                                  )
    
    # define the CNN model architecture
    model_hyperparameters = {
        "kernel_size" : 3,
        "out_channels" : 3
    }
    model = Simple_CNN(input_dim = NUM_FEATURES
                       , in_channels = 3
                       , output_dim = 2
                       , hyperparams = model_hyperparameters).to(device)

    # setup training    
    training_hyperparameters = {
        "learning_rate" : 0.001,
        "num_epochs" : 20,
        "milestones" : [35, 70, 90]
    }
    model.fit(train_loader = train_dataloader, test_loader = test_dataloader, hyperparams = training_hyperparameters)
    
    # checkpoint
    # TODO: save ADAM optimizer state?
    torch.save(model, 'FFN_balanced_class.ckpt')
    print("** Trained model saved to disk! **")
        
    print ("** Done! **")
