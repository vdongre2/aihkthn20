# Cloud-on-U-Net - NCSA Hackathon III Spring 2020

 Cloud-on-U-Net  is U-Net trained on MODIS Band 2, 31, 26

## File Descriptions

1. `extract_modis.py`

    Save each MODIS data and label as seperate files

2. `train.py`

    Train dataset using U-Net

3. `eval.py`
	Evaluate model

Trained weight can be accessed from: https://drive.google.com/file/d/1vHt4qOTYCX1VnKXVWKBqi4h70hipQO2K/view?usp=sharing
    