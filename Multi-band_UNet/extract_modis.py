import h5py
import os
import numpy as np
from tqdm import tqdm
giant_array = []
giant_label_array = []
home = '/home/hackathon/output_64_Javier_labelled'
for path in tqdm(os.scandir(home)):
    if path.path.endswith(".hdf"):
       file_path = str(path.path)
    else:
       continue
    try:
       #grab h5py file object
       hf_file = h5py.File(file_path, 'r')
    except:
       continue
    #list the main groups; image number in this case
    hf_keys = list(hf_file.keys())
    #access all data within images; save into an array if you like
    #automatically extracted as numpy arrays
    for image_num in hf_keys:
        Classification_Accuracy = hf_file[image_num + '/ClassificationAccuracy'][()]
        Feature_Labels          = hf_file[image_num + '/FeatureLabels'][()]
        Image_Classification    = hf_file[image_num + '/ImageClassification'][()]
        Image_Features          = hf_file[image_num + '/ImageFeatures'][()]

        if Classification_Accuracy == 0:
          continue
        
        np.save('/home/qgao10/modis_data/'+str(image_num), Image_Classification)
        np.save('/home/qgao10/modis_label/'+str(image_num), Image_Features)


