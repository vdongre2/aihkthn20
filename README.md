# NimbusNet - NCSA Hackathon III Spring 2020

NimbusNet is cloud detection project powered by various state-of-the-art deep learning algorithms.

## Best Results

Our best results were obtained using a Dynamic UNet model based on ResNet-18 with Self Attention and Mish activation function. We combined spectral bands 2, 26, and 32 in the form of an RGB image and used about 52,000 blocks of 64 x 64 pixels for training. The code and results of using this model are present in `Dynamic_UNet` folder.

## Presentation URL
https://docs.google.com/presentation/d/1Z8NWa7d3i71DCaO3xTk0UDyqK2bhMog3C_FTIU87yS0/edit?usp=sharing

## File/Folder Descriptions

1. `count_blocks.py`

    Get some basic stats about the data. E.g., total number of blocks, the ones that are manually classified as good training data and bad training data.

2. `FFN/create_dataset_FFN.py`

    Create dataset for pixel-wise binary classification (Feed Forward Network (FFN) / Ensemble )

3. `Dynamic_UNet/create_dataset_UNet.py`

    Create dataset for UNet

4. `FFN/train_FFN.py`

    Train dataset using FFN

5. `Dynamic_UNet/train_UNet.py`

    Train dataset using UNet
    
6. `Bagging.ipynb`

    Contains ensemble of decision trees based on bagging and random patching
    
7. `NCSA Hackathon III`

    Final Presentation file

8. `Multi-band_UNet` contains Non-dynamic UNet model base on band 2, 26 and 31, for more details check README in the folder.