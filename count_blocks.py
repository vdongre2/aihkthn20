import os
import h5py
import numpy

home = '/home/hackathon/output_64_Javier_labelled/'
good_block_count = 0
total_block_count = 0
bad_block_count = 0
file_count = 0

for filename in os.listdir(home):
    file_path = os.path.join(home, filename)

    # Ignore directories
    if os.path.isdir(file_path):
        continue

    file_count += 1

    # grab h5py file object
    try:
        hf_file = h5py.File(file_path, 'r')

        # list the main groups; image number in this case
        hf_keys = list(hf_file.keys())

        # access all data within images; save into an array if you like
        # automatically extracted as numpy arrays
        for image_num in hf_keys:
            total_block_count += 1
            Classification_Accuracy = hf_file[image_num + '/ClassificationAccuracy'][()]
            if Classification_Accuracy == 1:
                Feature_Labels = hf_file[image_num + '/FeatureLabels'][()]
                Image_Classification = hf_file[image_num + '/ImageClassification'][()]
                Image_Features = hf_file[image_num + '/ImageFeatures'][()]
                good_block_count += 1
            elif Classification_Accuracy == 0:
                bad_block_count += 1
    except Exception as ex:
        continue

print("good_block_count:", good_block_count)
print("total_block_count:", total_block_count)
print("bad_block_count:", bad_block_count)
print("file_count:", file_count)